from random import choice as random_choice, shuffle
import datetime


def user_choice(*variants):
    """
    Function helps user to chose only one from defined variants.
    Args:
        variants (str):
    Returns:
        str
    """
    msg = f'Choose one of: {", ".join(variants)}: \n'
    while True:
        user_input = input(msg)
        if user_input not in variants:
            print('Wrong!')
        else:
            return user_input


def computer_choice(*variants):
    """
    Random choosing one of the variants
    Args
        variants (str):
    Returns:
        str
    """
    return random_choice(variants)


def results_to_file(massage, raw_name="results.log"):
    """
    Function saves results of the game with actual data and time
    Args:
        massage (str):
        raw_name (str):
    """
    date_and_time = datetime.datetime.today()
    try:
        f_name = open(str(raw_name), "r")
        current_str = str((len(list(f_name))) + 1)
    except FileNotFoundError:
        current_str = "1"

    f_name = open(str(raw_name), "a")
    f_name.write(f"{current_str} {date_and_time} {massage}")
