# Написать функцию, принимающую два аргумента. Функция должна :
# - если оба аргумента относятся к числовым типам - вернуть их произведение,
# - если к строкам - соединить в одну строку и вернуть,
# - если первый строка, а второй нет - вернуть словарь (dict), в котором ключ - первый аргумент, значение - второй
# в любом другом случае вернуть кортеж (tuple) из аргументов

def type_function(arg1=None, arg2=None):

    if isinstance(arg1, (int, float, complex)) and isinstance(arg2, (int, float, complex)):
        return arg1*arg2
    elif isinstance(arg1, str) and isinstance(arg2, str):
        return f"{arg1}{arg2}"
    elif isinstance(arg1, str) and not isinstance(arg2, str):
        return {
            arg1: arg2
        }
    else:
        return arg1, arg2


# --------------------------------------------------------------------------------------------------------------------

# Пользователь вводит строку произвольной длины. Написать функцию, которая принимает строку и должна вернуть словарь
# следующего содержания:
# ключ - количество букв в слове, значение - list слов с таким количеством букв.
# отдельным ключем, например "0", записать количество пробелов.
# отдельным ключем, например "punctuation", записать все уникальные знаки препинания, которые есть в тексте.

def str_analysis(arg):
    """
    Function returns dict with words sorted by length. Separately sorted whitespaces & punctuation symbols.
    Items containing words sorted by length are generated automatically.

    :param arg: str, inputted by user text
    :return: dict
    """
    some_string = str(arg)

    # Создание базового словаря
    analysis_results = {
        "whitespaces": 0,
        "digits": set(),
        "punctuation": set(),
    }

    # Наполнение сетов словаря уникальными значениями символов/цифер и чистка строки для анализа слов
    counter = 0
    for character in some_string:
        if character == " ":
            analysis_results["whitespaces"] += 1
        elif character.isdigit():
            analysis_results["digits"].add(character)
            some_string = some_string.replace(some_string[counter], ' ')
        elif not character.isalpha():
            analysis_results["punctuation"].add(character)
            some_string = some_string.replace(some_string[counter], ' ')
        counter += 1

    #  Разбивка строки на слова и сортировка в списке по длине строки
    list_from_string = some_string.lower().split()
    list_from_string.sort(key=len)

    # Генерация элемента слова для каждого нового максимального кол-ва символов в слове.
    max_len = 0
    for word in list_from_string:
        word_length = len(word)
        if word_length > max_len:
            analysis_results[word_length] = [word]
            max_len = word_length
        else:
            analysis_results[word_length].append(word)

    return analysis_results
