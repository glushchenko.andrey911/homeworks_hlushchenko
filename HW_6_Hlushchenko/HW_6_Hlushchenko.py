# В отдельном файле (пусть будет lib.py) написать функцию, которая требует от пользователя ответить да или нет (Y/N)
# и возвращает True/False в зависимости от того, что он ввел. В основном файле (пусть будет main_file.py) попросить
# пользователя ввести с клавиатуры строку и вывести ее на экран. Используя импортированную из lib.py функцию спросить
# у пользователя, хочет ли он повторить операцию (Y/N). Повторять пока пользователь отвечает Y и прекратить когда
# пользователь скажет N.
from lib import yes_or_no


def input_repeat():
    user_input = input("Please type something")
    want_to_repeat = True

    while len(user_input) == 0:
        user_input = input("Please do not leave blank")

    while want_to_repeat:
        print(user_input)
        want_to_repeat = yes_or_no()


input_repeat()

# Модифицируем ДЗ 2. Напишите с помощью функций!. Помните о Single Responsibility! Попросить ввести свой возраст
# (можно использовать константу или input()). Пользователь ввел значение возраста [year number] а на место [year string]
# нужно поставить правильный падеж существительного "год", который зависит от значения [year number].
# если пользователь ввел непонятные данные (ничего не ввел, ввел не число, неактуальный возраст)- вывести “не понимаю”
# если пользователю меньше 7 - вывести “Тебе [year number] [year string], где твои мама и папа?”
# если пользователю меньше 18 - вывести “Тебе [year number] [year string], а мы не продаем сигареты несовершеннолетним”
# если пользователю больше 65 - вывести “Вам уже [year number] [year string], вы в зоне риска”!
# в любом другом случае - вывести “Наденьте маску, вам же [year number] [year string]!”


def age_input():
    """
    Returns age value in case that a number was entered by user.
    :return: int
    """
    answer = input("Cколько вам лет?")

    while not answer.isdigit() or answer == "0":
        answer = input("Столько лет не бывает! Сколько вам полных лет на самом деле?")

    age = int(answer)
    return age


def case_by_age(age):
    """
    Returns word "год" with case matching the entered age
    :param age: int
    :return: str
    """
    case_test = age % 10
    extra_old_case_test = age % 100

    if extra_old_case_test in range(11, 15):
        return "лет"
    elif case_test == 1:
        return "год"
    elif case_test in (2, 3, 4):
        return "года"
    else:
        return "лет"


def answer_by_age():
    """
    Returns phrase according to the entered age
    :return:
    """
    age = age_input()
    age_case = case_by_age(age)

    if age < 7:
        return f"Тебе всего {age} {age_case}, где твои мама и папа?"
    elif age < 18:
        return f"Тебе всего {age} {age_case}, a мы не продаем сигареты несовершеннолетним?"
    elif age > 65:
        return f"Вам уже {age} {age_case}, вы в зоне риска!"
    else:
        return f"Наденьте маску, вам же {age} {age_case}!"


print(answer_by_age())
