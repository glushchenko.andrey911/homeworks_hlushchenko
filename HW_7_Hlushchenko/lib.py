from random import randint


def user_number(max_value):
    """
    Function for converting user input to integer in range from 1 to max_value.
    :param max_value: int
    :return: int
    """
    while True:
        try:
            num = int(input(f"Input the number from 1 to {max_value}"))
        except ValueError:
            print("Not a number!")
            continue

        if max_value < num or num < 1:
            print(f"Your number is out of play range!")
            continue
        else:
            return num


def computer_number(max_value):
    """
    Function that returns random integer in range from 1 to max_value.
    :param max_value: int
    :return: int
    """
    num = randint(1, max_value)
    return num


def smart_comparison(user_num, computer_num, max_value):
    """
    Function compares two values & gives a hint about a difference between them.
    :param user_num: int
    :param computer_num: int
    :param max_value: int
    :return: bool
    """
    proximity = abs(computer_num - user_num)
    if proximity == 0:
        return True
    if 1 <= proximity <= max_value/100:
        print("BURNING!")
    elif max_value/100 < proximity <= max_value/25:
        print("Hot")
    elif max_value/25 < proximity <= max_value/10:
        print("Warm")
    else:
        print("Cold")
