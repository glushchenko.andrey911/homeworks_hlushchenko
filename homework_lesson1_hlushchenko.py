# Задача 1: Создать две переменные first=10, second=30.
# Вывести на экран результат математического взаимодействия (+, -, *, / и тд.) для этих чисел.
# Решил сделать немного более интерактивно и заодно дать лучше понять уровень подготовки группы и себя в ее числе.

print('')
print ('Привет. Давай поработаем с числами!\n')
first = int (input ('Введи первое целое число:'))
second = int (input ('Введи второе целое число:'))
print('')
add = first + second
print ('Сложение: ' + str(add))
substr = first - second
print ('Вычитание: ' + str(substr))
div = first / second
print ('Деление: ' + str(div))
fl_div = first // second
print ('Целочисленное деление: ' + str(fl_div))
modulus = first % second
print ('Остаток от деления: ' + str(modulus))
mult = first * second
print ('Умножение: ' + str(mult))
exp = first ** second
print ('Возведение в степень: ' + str(exp))
print('')

#Задача 2:Создать переменную и поочередно записать в нее результат сравнения (<, > , ==, !=) чисел из задания 1.
#После этого вывести на экран значение каждой полученной переменной.

solution = first > second
print ('Первое число больше второго - ' + str(solution))
solution = first < second
print ('Первое число меньше второго - ' + str(solution))
solution = first == second
print ('Первое число равно второму - ' + str(solution))
solution = first != second
print ('Первое число не равно второму - ' + str(solution))
print('')

#Задача 3: Создать переменную - результат конкатенации (сложения) строк str1="Hello " и str2="world". Вывести на ее экран.

str1 = 'Hello '
str2 = 'world :)'
title = str1 + str2
print (title)
