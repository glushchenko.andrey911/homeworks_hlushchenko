# Подключіться до API НБУ ( документація тут https://bank.gov.ua/ua/open-data/api-dev ), отримайте теперішній курс
# валют и запишіть його в TXT-файл в такому форматі:
#  "[дата, на яку актуальний курс]"
# 1. [назва валюти 1] to UAH: [значення курсу валюти 1]
# 2. [назва валюти 2] to UAH: [значення курсу валюти 2]
# 3. [назва валюти 3] to UAH: [значення курсу валюти 3]
# ...
# n. [назва валюти n] to UAH: [значення курсу валюти n]
#
# опціонально передбачте для користувача можливість обирати дату, на яку він хоче отримати курс

import requests
import datetime
from re import search as regex_search


class UAHCurrencyRate:
    """
    This class may be used for requesting data about currency rate of Hryvna from official NBU resource.

    Date can be changed to specific necessary values. Use date in format "YYYYMMDD"

    Rates data can be saved to the file with specified filename in format (1. USD to UAH: "rate value"...) with
    "save_to_file" method or Currency Rate of Hryvna to specified Currency can be searched with "selected_rate" method.
    """

    __today = datetime.datetime.now().strftime("%Y%m%d")
    __url = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date={}&json"
    __rates_list = []
    __filename_rule = r'^\w+.txt$'
    __filename = None

    def __init__(self,
                 date: str = __today,
                 currency_to_compare: str = "USD",
                 filename: str = 'UAHCurrencyRate.txt'):

        self.date = date
        self.currency_to_compare = currency_to_compare
        self.__filename = filename

    @property
    def date(self):
        return self.__date

    @date.setter
    def date(self, value):
        try:
            datetime.datetime.strptime(value, '%Y%m%d')
        except Exception:
            raise TypeError("Enter date in format \'yyyymmdd\'!")  #
        self.__date = value

    @property
    def currency_to_compare(self):
        return self.__currency_to_compare

    @currency_to_compare.setter
    def currency_to_compare(self, value: str):
        if not isinstance(value, str) or not value.isalpha() or len(value) != 3:
            raise TypeError('Enter currency to compare in format \'XXX\'! Use just letters!')
        self.__currency_to_compare = value.upper()

    @property
    def filename(self):
        return self.__filename

    @filename.setter
    def filename(self, value: str):
        if not regex_search(self.__filename_rule, value):
            raise TypeError('Incorrect filename!')
        self.__filename = value

    def update_info(self):
        """ Method to request and validate data from defined url"""

        try:
            res = requests.get(self.__url.format(self.date))
        except Exception as e:
            print(e)
        else:
            if 300 > res.status_code >= 200 and res.headers.get('Content-Type',
                                                                None) == 'application/json; charset=utf-8':
                self.__rates_list = res.json()
            else:
                return res.text

    def save_to_file(self):
        """ Method may be used to save UAH currency rate to TXT file in format: (1. USD to UAH: "rate value"...)"""

        counter = 0
        with open(self.filename, 'w') as file:
            date_formated = f'{self.date[6:8]}-{self.date[4:6]}-{self.date[0:4]}'
            file.write(f'UAH Currency Rate on Date: {date_formated} \n \n')
            for item in self.__rates_list:
                counter += 1
                file.write(f"{counter}. {item.get('cc')} to UAH: {item.get('rate')} \n")

    def selected_rate(self):
        """
         Method to get rate value based on specified currency
        :return: str
        """
        for item in self.__rates_list:
            if item.get('cc') == self.currency_to_compare:
                return item.get('rate')
            else:
                return 'Wrong currency index'

# here are some QA probes

# a = UAHCurrencyRate("20210330", 'EUR')
# a.save_to_file()
# a.currency_to_compare = "aud"
# a.date = "20200220"
# print(a.date)
# print(a.currency_to_compare)
# a.update_info()
# print(a.selected_rate())
# a.filename = "NewRatesList.txt"
# a.save_to_file()
